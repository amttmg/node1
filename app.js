import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

import userRoutes from './app/api/routes/users.route';
import authRoute from './app/api/auth/auth';

const app = express();

app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({limit: '20mb', extended: false}));

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/delivery_db', {
  useMongoClient: true,
});

app.use('/api/', authRoute);
app.use('/api/users', userRoutes);

export default app;