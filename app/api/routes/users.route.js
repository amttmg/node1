import express from 'express';
import * as usersController from '../controllers/users.controller';

const router = new express.Router();

router.route('/').post(usersController.list);

router.route('/store').post(usersController.store);

router.route('/:userId').get(usersController.show);

router.route('/:userId').patch(usersController.update);

router.route('/:userId').delete(usersController.destroy);

export default router;
