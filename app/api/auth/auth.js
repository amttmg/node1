import express from 'express';
import User from '../models/users.model';

const router = express.Router();

router.post('/auth', (req, res) => {
  const {credentials} = req.body;
  User.findOne({email: credentials.email}).then(User => {
    if (User && User.isValidPassword(credentials.password)) {
      res.status(200).json({user: User.toAuthJSON()});
    } else {
      res.status(400).json({errors: {global: "Invalid username password"}});
    }
  });
});

export default router;