import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const Schema = mongoose.Schema;

const usersSchema = new Schema({
    name: {type: String, require: true},
    address: String,
    phone: {type: Number, require: true, lowercase: true},
    email: {type: String, require: true, lowercase: true},
    password: {type: String, require: true, lowercase: true},
    status: {type: Number, require: true, lowercase: true},
  },
  {timestamp: true}
);

usersSchema.methods.isValidPassword = function isValidPassword(password) {
  return password === this.password;
};

usersSchema.methods.generateJWT = function generateJWT() {
  return jwt.sign({
    email: this.email
  }, 'secreteKey')
};

usersSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    email: this.email,
    token: this.generateJWT()
  }
};

export default mongoose.model('User', usersSchema);
