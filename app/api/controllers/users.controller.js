import User from '../models/users.model';

function handleError(res, err) {
  return res.send(500, err);
}

export function list(req, res) {
  const {pageSize, page, sorted, filtered} = req.body.requestData;
  const query = User.find().sort().skip(page * pageSize).limit(pageSize);
  query.exec((Err, Users) => {
    return res.json(200, {
      data: Users,
      page: 2
    });
  });
}


export function show(req, res) {
  User.findById(req.params.userId, (err, users) => {
    if (err) {
      return handleError(res, err);
    }
    if (!users) {
      return res.send(404);
    }
    return res.json(users);
  });
}


export function store(req, res) {
  const user = new User({
    name: req.body.name,
    address: req.body.address,
    phone: req.body.phone,
    email: req.body.email,
    password: req.body.password,
    status: 1,
    dates: {
      created_at: Date.now(),
      updated_at: null,
      deleted_at: null,
    }
  });
  user.save()
    .then(() => {
      res.status(200).json({
        message: 'Success',
      });
    })
    .catch((error) => {
      console.log(error);
      res.status(200).json({
        message: 'fail',
      });
    });
}

export function update(req, res) {
  const userId = req.params.userId;
  User.findById(userId, (err, user) => {
    if (err) {
      return handleError(res, err);
    }
    const userData = {
      name: req.body.name,
      address: req.body.address,
      phone: req.body.phone,
      email: req.body.email,
      password: req.body.password,
      status: 1,
      dates: {
        created_at: Date.now(),
        updated_at: null,
        deleted_at: null,
      }
    };
    user.update(userData).exec();
    res.status(200).json({
      message: "success",
    });
  });
}

export function destroy(req, res) {
  const userId = req.params.userId;
  res.status(200).json({
    message: `student for delete Id ${userId}`,
  });
}
