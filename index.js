import http from 'http';
import app from './app';
const port = 3030;
http.createServer(app).listen(port, '127.0.0.1');

console.log('Server running at http://127.0.0.1:3030/');